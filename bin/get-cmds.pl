#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use JSON::XS;

my ( $file, @format, $header, %format, $dest, @h, %a, $i, $x, $status, $signal );
my ( $cmd, $j, $json, $cmdStash, @cmdHeader, $jobID, $out, %c, $help, $nocmd );
my ( $ePerFile, $eRead, $eWritten );

$header = "analysis/header.csv";

$out = "analysis/data/jgi-qos.csv";
$cmdStash = "analysis/data/jgi-qos-cmds.csv";
GetOptions(
            "help"  => \$help,
            "out=s" => \$out,
            "cmd=s" => \$cmdStash,
            "nocmd" => \$nocmd,
          );

$help && die <<EOF;

  Usage: $0 [--out=<filename>] [--cmds[<filename>] [--nocmd] file1 file2 file3 ...

  where:

  --out is the name of the file to dump the slurm accounting info

  --cmds is the name of the file for the AdminComment per job (slow!)

  --nocmd suppresses looking up the AdminComment per job

 file1,2,3 are the input csv files from get-stats.sh

EOF

@cmdHeader = qw[ jobId
                 arrayJobId
                 arrayTaskId
                 jobAccount
                 workingDirectory
                 stdoutPath
                 stderrPath
                 features
               ];

open HDR, "< $header" or die "$header: $!\n";
$_ = <HDR>;
close HDR;
chomp;
@format = split( ',', $_ );
push( @format,
          qw[
              ExitSignal
              DerivedExitSignal
              ReqMemPer
              Duration
              Step
            ]
    );
 
$i = 0;
map { $format{ $_ } = $i++ } @format;

sub parseIO {
  my $io = shift;
  my ($val,$unit);

  return 0 unless $io;

  $io =~ m%^([0-9]+(\.[0-9]*)?)([^0-9]+)$%;
  $val = $1;
  $unit = $3;
  if ( $unit eq 'K' ) { return $val / 1024; }
  if ( $unit eq 'M' ) { return $val; }
  if ( $unit eq 'G' ) { return $val * 1024; }
  if ( $unit eq 'T' ) { return $val * 1024 * 1024; }
  if ( $unit eq 'P' ) { return $val *  1024 * 1024 * 1024; }
}

sub parseTime {
  my $t = shift;
  my ($days,$hours,$minutes,$seconds);

  return 0 unless $t;
  $t =~ s%-%:%;
  my @a = split( ':', $t );
  $seconds = pop( @a );
  $minutes = ( pop( @a ) || 0 );
  $hours   = ( pop( @a ) || 0 );
  $days    = ( pop( @a ) || 0 );

  $t = $days * 86400 + $hours * 3600 + $minutes * 60 + $seconds;
  return $t;
}

if ( ! -f $out ) {
  open OUT, ">$out" or die "CREATE: $out: $!\n";
  print OUT join( ';', @format ), "\n";
} else {
  open IN, "< $out" or die "IN: $out: $!\n";
  while ( <IN> ) {
    chomp;
    $c{ $_ } ++;
  }
  close IN;
  open OUT, ">> $out" or die "APPEND: $out: $!\n";
}

if ( ! $nocmd ) {
  if ( ! -f $cmdStash ) {
    open CMDS, ">$cmdStash" or die "$cmdStash: $!\n";
    print CMDS join( '|', @cmdHeader ), "\n";
  } else {
    open CMDS, ">> $cmdStash" or die "$cmdStash: $!\n";
  }
}

$ePerFile = $eRead = $eWritten = 0;

while ( $file = shift ) {
  -f $file or die "No such file: $file\n";

  print STDERR $file, ": ";
  if ( $file =~ m%\.gz$% ) {
    open IN, "cat $file | gzip -d - |" or die "Read $file: $!\n";
  } elsif ( $file =~ m%\.xz$% ) {
    open IN, "cat $file | xz -d - |" or die "Read $file: $!\n";
  } elsif ( $file =~ m%\.bz2$% ) {
    open IN, "cat $file | bzip2 -d - |" or die "Read $file: $!\n";
  } else {
    open IN, "< $file" or die "Read $file: $!\n";
  }
  while ( <IN> ) {
    chomp;
    if ( exists( $c{ $_ } ) ) {
      next;
    } else {
      $c{ $_ }++;
    }
    undef @h;
    @h = split( '\|' );

    $eRead++;

    next if $h[ $format{ TotalCPU } ] eq "00:00:00";

#
#   Many times are in D-HH:MM:SS format
    foreach ( qw / AveCPU MinCPU SystemCPU Timelimit TotalCPU UserCPU Suspended / ) {
      $h[ $format{ $_ } ] = parseTime( $h[ $format{ $_ } ] );
    }

#
#   Data I/O is in K/M/G/T format
    foreach ( qw / AveCPUFreq AveDiskRead AveDiskWrite AvePages AveRSS AveVMSize MaxDiskRead MaxDiskWrite MaxPages MaxRSS MaxVMSize / ) {
      $h[ $format{ $_ } ] = parseIO( $h[ $format{ $_ } ] );
    }

#
#   ExitCode & DerivedExitCode are (status):(signal)
    foreach ( qw / ExitCode DerivedExitCode / ) {
      $x = $h[ $format{ $_ } ];
      $x =~ m%^(\d+):(\d+)$%;
      $status = $1 || 0;
      $signal = $2 || 0;
      $h[ $format{ $_ } ] = $status;
      $i = $_;
      $i =~ s%Code%Signal%;
      $h[ $format{ $i } ] = $signal;
    }

#
#   ReqMem is nnnnM(c|n), MB per Core or MB per Node
    $i = $h[ $format{ ReqMem } ];
    $i =~ m%^(\d+)M(.)$%;
    my $r = $1;
    my $rp = $2;
    $h[ $format{ ReqMem } ] = $r;
    $h[ $format{ ReqMemPer } ] = $rp;

    $h[ $format{ Duration } ] = $h[ $format{ End } ] - $h[ $format{ Start } ];

    $i = $h[ $format{ JobIDRaw } ];
    if ( $i =~ m%^(\d+)\.(.+)$% ) {
      $h[ $format{ JobIDRaw } ] = $1;
      $h[ $format{ Step } ] = $2;
    }

    $_ = join( ";", @h );
    if ( exists( $c{ $_ } ) ) {
      next;
    } else {
      $c{ $_ }++;
    }
    print OUT $_, "\n";
    $ePerFile++;
    $eWritten++;

#
#   Get the command, stdout and stderr, if I can
    if ( ! $nocmd ) {
      $jobID = $h[ $format{ JobIDRaw } ];
      if ( $jobID =~ m%^[0-9]+$% ) {
        $cmd = "sacct --format=AdminComment -j " . $h[ $format{ JobIDRaw } ] . " -P";
        open JSON, "$cmd | grep -v AdminComment |" or die "$cmd: $!\n";
        while ( $json = <JSON> ) {
          if ( $json =~ m%^\S+$% ) {
            $j = decode_json( $json );
            print CMDS join( '|', map { $j->{$_} || '' } @cmdHeader ), "\n";
          }
        }
        close JSON or die "close: $cmd: $!\n";
      }
    }
  }

  print STDERR $ePerFile, " entries\n";
}

print STDERR "Total entries read: ", $eRead, "\n";
print STDERR "Total entries written: ", $eWritten, "\n";
