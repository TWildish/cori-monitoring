#!/bin/bash
cd `dirname $0`

#format=User,JobName,JobIDRaw,Start,End,ExitCode,AllocCPUS,AllocNodes,AveCPU,AveCPUFreq,AveDiskRead,AveDiskWrite,AvePages,AveRSS,AveVMSize,CPUTimeRAW,ConsumedEnergyRaw,DerivedExitCode,ElapsedRaw,MaxDiskRead,MaxDiskReadNode,MaxDiskReadTask,MaxDiskWrite,MaxDiskWriteNode,MaxDiskWriteTask,MaxPages,MaxPagesNode,MaxPagesTask,MaxRSS,MaxRSSNode,MaxRSSTask,MaxVMSize,MaxVMSizeNode,MaxVMSizeTask,MinCPU,MinCPUNode,MinCPUTask,NNodes,NTasks,NodeList,Partition,Priority,QOS,ReqCPUS,ReqMem,ReqNodes,State,Submit,Suspended,SystemCPU,Timelimit,TotalCPU,UserCPU
#sacct --user=wildish --starttime 2017-09-01T00:00:00 --endtime now --parsable --noconvert --noheader --format=$format | tee wildish.csv

rm -rf ../raw-data/cori-wildish
NERSC_HOST="cori-wildish" \
HOST_FLAGS="--user=wildish" \
origin="2017-10-10" \
./get-stats.sh
