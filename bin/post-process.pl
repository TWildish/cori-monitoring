#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use JSON::XS;
use File::Basename;

my ( $file, @format, $header, %format, $dest, @h, %a, $i, $x, $status, $signal );
my ( $cmd, $j, $json, %j, $JobIDRaw, $Step, $out, %c, $help );
my ( $ePerFile, $eRead, $eWritten );

$header = ( dirname $0 ) . "/../analysis/header.csv";

GetOptions(
            "help"  => \$help,
            "out=s" => \$out,
          );

defined $out or die "\"--out=<filename>\" is obligatory\n";

$help && die <<EOF;

  Usage: $0 [--out=<filename>] [--cmds[<filename>] [--nocmd] file1 file2 file3 ...

  where:

  --out is the name of the file to dump the slurm accounting info

 file1,2,3 are the input csv files from get-stats.sh

EOF

open HDR, "< $header" or die "$header: $!\n";
$_ = <HDR>;
close HDR;
chomp;
@format = split( ',', $_ );
push( @format,
          qw[
              ExitSignal
              DerivedExitSignal
              ReqMemPer
              Step
            ]
    );

$i = 0;
map { $format{ $_ } = $i++ } @format;

sub parseIO {
  my $io = shift;
  my ($val,$unit);

  return 0 unless $io;

  $io =~ m%^([0-9]+(\.[0-9]*)?)([^0-9]+)$%;
  $val = $1;
  $unit = $3;
  if ( $unit eq 'K' ) { return $val / 1024; }
  if ( $unit eq 'M' ) { return $val; }
  if ( $unit eq 'G' ) { return $val * 1024; }
  if ( $unit eq 'T' ) { return $val * 1024 * 1024; }
  if ( $unit eq 'P' ) { return $val *  1024 * 1024 * 1024; }
}

sub parseTime {
  my $t = shift;
  my ($days,$hours,$minutes,$seconds);

  return 0 unless $t;
  $t =~ s%-%:%;
  my @a = split( ':', $t );
  $seconds = pop( @a );
  $minutes = ( pop( @a ) || 0 );
  $hours   = ( pop( @a ) || 0 );
  $days    = ( pop( @a ) || 0 );

  $t = $days * 86400 + $hours * 3600 + $minutes * 60 + $seconds;
  return $t;
}

#
# Here starts the main program...

#
# Create the output file, with header, if it doesn't exist. Otherwise,
# map the fields into the cache, so I don't duplicate them
if ( ! -f $out ) {
  open OUT, ">$out" or die "CREATE: $out: $!\n";
  print OUT join( ';', @format ), "\n";
} else {
  open IN, "< $out" or die "IN: $out: $!\n";
  while ( <IN> ) {
    chomp;
    $c{ $_ } ++;
  }
  close IN;
  open OUT, ">> $out" or die "APPEND: $out: $!\n";
}

$ePerFile = $eRead = $eWritten = 0;

while ( $file = shift ) {
  -f $file or die "No such file: $file\n";

  print STDERR $file, ": ";
  if ( $file =~ m%\.gz$% ) {
    open IN, "cat $file | gzip -d - |" or die "Read $file: $!\n";
  } elsif ( $file =~ m%\.xz$% ) {
    open IN, "cat $file | xz -d - |" or die "Read $file: $!\n";
  } elsif ( $file =~ m%\.bz2$% ) {
    open IN, "cat $file | bzip2 -d - |" or die "Read $file: $!\n";
  } else {
    open IN, "< $file" or die "Read $file: $!\n";
  }

  while ( <IN> ) {
    chomp;
    if ( exists( $c{ $_ } ) ) {
      next;
    } else {
      $c{ $_ }++;
    }

    undef @h;
    @h = split( '\|' );
    next if $h[ $format{ End } ] eq 'Unknown'; # Job is still running
    $eRead++;

    $i = $h[ $format{ JobIDRaw } ];
    $h[ $format{ Step } ] = '';
    if ( $i =~ m%^(\d+)\.(.+)$% ) {
      $h[ $format{ JobIDRaw } ] = $1;
      $h[ $format{ Step } ] = $2;
    }

#
#   Many times are in D-HH:MM:SS format
    foreach ( qw / AveCPU MinCPU SystemCPU Timelimit TotalCPU UserCPU Suspended / ) {
      defined( $h[ $format{ $_ } ] ) || goto END_OF_LOOP;
      $h[ $format{ $_ } ] = parseTime( $h[ $format{ $_ } ] );
    }

#
#   Data I/O is in K/M/G/T format
    foreach ( qw / AveCPUFreq AveDiskRead AveDiskWrite AvePages AveRSS AveVMSize MaxDiskRead MaxDiskWrite MaxPages MaxRSS MaxVMSize / ) {
      $h[ $format{ $_ } ] = parseIO( $h[ $format{ $_ } ] );
    }

#
#   ExitCode & DerivedExitCode are (status):(signal)
    foreach ( qw / ExitCode DerivedExitCode / ) {
      $x = $h[ $format{ $_ } ];
      $x =~ m%^(\d+):(\d+)$%;
      $status = $1 || 0;
      $signal = $2 || 0;
      $h[ $format{ $_ } ] = $status;
      $i = $_;
      $i =~ s%Code%Signal%;
      $h[ $format{ $i } ] = $signal;
    }

#
#   ReqMem is nnnnM(c|n), MB per Core or MB per Node
    $i = $h[ $format{ ReqMem } ];
    $i =~ m%^(\d+)M(.)$%;
    my $r = $1 || 0;
    my $rp = $2 || 0;
    $h[ $format{ ReqMem } ] = $r;
    $h[ $format{ ReqMemPer } ] = $rp;

    $JobIDRaw = $h[ $format{ JobIDRaw } ] . $h[ $format{ Step } ];
    push @{ $j{ $JobIDRaw } }, @h;
    $ePerFile++;
END_OF_LOOP:
  }

  print STDERR $ePerFile, " entries\n";
}

foreach ( values %j ) {
  print OUT join( ";", @{ $_ } ), "\n";
  $eWritten++;
}

print STDERR "Total entries read: ", $eRead, "\n";
print STDERR "Total entries written: ", $eWritten, "\n";
