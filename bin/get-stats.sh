#!/bin/bash -l

cd `dirname $0`/..
set -e

format=User,Account,JobName,JobIDRaw,Start,End,ExitCode,AllocCPUS,AllocNodes,AveCPU,AveCPUFreq,AveDiskRead,AveDiskWrite,AvePages,AveRSS,AveVMSize,CPUTimeRAW,ConsumedEnergyRaw,DerivedExitCode,ElapsedRaw,MaxDiskRead,MaxDiskReadNode,MaxDiskReadTask,MaxDiskWrite,MaxDiskWriteNode,MaxDiskWriteTask,MaxPages,MaxPagesNode,MaxPagesTask,MaxRSS,MaxRSSNode,MaxRSSTask,MaxVMSize,MaxVMSizeNode,MaxVMSizeTask,MinCPU,MinCPUNode,MinCPUTask,NNodes,NTasks,NodeList,Partition,Priority,QOS,ReqCPUS,ReqMem,ReqNodes,State,Submit,Suspended,SystemCPU,Timelimit,TotalCPU,UserCPU
# --units={KMGTP]

export SLURM_TIME_FORMAT="%Y-%m-%d %H:%M:%S" # This is standard
export SLURM_TIME_FORMAT="%s" # This is epoch time

if [ -z "$NERSC_HOST" ] || [ "$NERSC_HOST" == 'unknown' ]; then
  my_host=`cat /etc/clustername`
  if [ ! -z "$my_host" ]; then
    export NERSC_HOST=$my_host
  fi
fi

case "$NERSC_HOST" in
  cori)
    HOST_FLAGS="--qos=jgi"
    origin=2017-05-01
    out=analysis/data/jgi-qos.csv
    module use /global/common/software/m342/Modules/usg
    ;;
  denovo)
    unset HOST_FLAGS
    origin=2017-10-01
    out=analysis/data/$NERSC_HOST.csv
    ;;
   "")
    unset HOST_FLAGS
    origin=2017-10-01
    export NERSC_HOST="unknown"
    ;;
   *)
    if [ "$origin" == "" ]; then
      origin=2017-10-01
    fi
    ;;
esac

echo "Running on $NERSC_HOST, with HOST_FLAGS=$HOST_FLAGS and origin=$origin"
data=raw-data/$NERSC_HOST/data
header=analysis/header.csv

#
# Today's data may be there, but incomplete. Fetch from scratch
today=`date +%Y-%m-%d`
f=$data/`date +%Y/%m`/sacct.$today.csv
[ -f $f ] && rm -f $f

terminate=0
i=1
while [ $terminate -eq 0 ]
do
  d=`date +%Y-%m-%d --date="- $i days"`
  ddir=$data/`date +%Y/%m --date="- $i days"`
  [ -d $ddir ] || mkdir -p $ddir
  if [ $d == $origin ]; then
    echo "Gone back to the origin ($origin), bailing out"
    exit 0
  fi
  echo "Check for $d"
  f=$ddir/sacct.$d.csv
  if [ -f $f ] || [ -f $ddir.csv ] || [ -f $ddir.csv.gz ]; then
    echo "Destination already exists, bailing out after this cycle"
    terminate=1
  fi
  i=`expr $i + 1`

  echo $format | tee $header >/dev/null
#
# Would like to add state flags to the following, but it doesn't work :-(
# --state=COMPLETED,COMPLETING,DEADLINE,FAILED,NODE_FAIL,PREEMPTED,RESIZING,SUSPENDED,TIMEOUT
# ...or...
# --state=CD,CG,DL,F,NF,PD,PR,RS,S,TO
  sacct \
    --starttime ${d}T00:00:00 --endtime ${d}T23:59:59 \
    --allusers \
    --parsable \
    --noconvert \
    --noheader \
    --format=$format \
      $HOST_FLAGS \
    | tee $f >/dev/null

done

if [ ! -z "$out" ]; then
  [ -f $out ] && rm -f $out
  module purge
  module load PrgEnv-gnu/7.1 bzip2/1.0.6 perl/5.24.0
  bin/post-process.pl --out=$out $data/*/*/sacct*.csv
  pbzip2 -f $out
fi
