rm( list=ls() )
require( dplyr )
#
# Pick our jobs that ran, successfully or not, and that took more than a few seconds
h.baseFilter <- function( data ) {
  return(
    ( data$State == 'COMPLETED' | data$State == 'FAILED' ) &
    ! data$ExitCode &
      data$ElapsedRaw > 300 &
      data$TotalCPU > 10
  )
}

#
# Find jobs by the numbers of steps they have
h.jobStepRange <- function( data, stepMin=1, stepMax=999999 ) {
  t <- aggregate(
          data$JobIDRaw,
          by=list( JobIDRaw=data$JobIDRaw ),
          FUN = function( x ) {
            return( length( x ) )
          }
        )
  data <- data[
      data$JobIDRaw %in% t$JobIDRaw[ which( t$x <= stepMax & t$x >= stepMin ) ]
    ,
  ]
  data$Step <- droplevels( data$Step )
  return( data )
}

#
# Find the busiest users, in terms of number of jobs
h.busyJobCount <- function( data, fraction=0.1 ) {
  data <- data[
                as.numeric( data$User ) %in%
                  which( table( data$User ) >
                    length( data$User ) * fraction )
                ,
              ]
  data$User <- droplevels( data$User )
  return( data )
}

#
# Add a few variables to any of the data-frames
h.calculateExtraFields <- function( data=NULL ) {
  data$logElapsed <- log10( data$Elapsed )

  data$CPUTimePerCPU <- data$TotalCPU / ( data$AllocCPUS )
  data$logCPUTimePerCPU <- log10( data$CPUTimePerCPU )

  data$CPUefficiency <- data$CPUTimePerCPU / data$ElapsedRaw

  data$SysFrac <- data$SystemCPU / data$TotalCPU

  return( data )
}

#
# Load a data-source, from the compile RData file if possible, or
# from CSV if not (and then cache it)
h.loadData <- function( name ) {
  CSVfile <- paste0(  "data/", name, ".csv" )
  RFile   <- paste0( "cache/", name, ".RData" )

  if ( file.exists( RFile ) ) {
    cat( "- Loading", RFile,"\n" )
    local( {
      load( RFile )
      return( data )
    } )
  } else {
    if ( file.exists( CSVfile ) ) {
      cat( "- Loading data from", CSVfile, "\n" )
      data <- read.csv( file=CSVfile, header=TRUE, sep=';' )
      data$Submit <- as.numeric( as.character( data$Submit ) )
      cat( "  - Data loaded, now de-duplicating\n" )
      data <- data[ !duplicated( data ), ]
      if ( 'Elapsed' %in% colnames( data ) ) {
        cat( "  - Adding extra data fields\n" )
        data <- h.calculateExtraFields( data=data )
      }
      cat( "  - Saving data to cache\n" )
      save( data, file=RFile )
      return( data )
    } else {
      cat( "*** No CSV file and no RData file for '", name, "', spitting the dummy\n" )
      return( )
    }
  }
}

#
# Filter out 'admin'-type users
h.removeUsers <- function( data, users=c( 'wildish', 'dudwary', 'kmfagnan', 'ebasheer', 'grath', 'root' ) ) {
  data <- data[ ! data$User %in% users, ]
  data$User <- droplevels( data$User )
  return( data )
}

cat( "Helper functions loaded:\n" )
cat( ls(), "\n" )